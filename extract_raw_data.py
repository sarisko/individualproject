import pickle
import numpy as np
import os
from pythoms.mzml import mzML 
from pythoms.mzml import extract_spectrum


directory_raw = 'C:\\Users\\Sarisko\\Documents\\Uni\\Indi_Project\\raw_files'
directory_processed = 'C:\\Users\\Sarisko\\Documents\\Uni\\Indi_Project\\processed_files'

for filename in os.listdir(directory_raw):
    if filename.endswith(".raw"):
        experiment_num = filename[:-4]
        # file is not already processed
        if not experiment_num+".p" in os.listdir(directory_processed):
            mzml = mzML(filename)
            mzml.function_timetic()
            timepoints = mzml.functions[1]['timepoints']

            # data = {m/z:[(t, i), (t, i), ...], ...}
            data = {}

            for spectrum_element, time_point in zip(mzml.tree.getElementsByTagName('spectrum'), timepoints):
                mz, intensity = extract_spectrum(spectrum_element)
                for single_ion, single_intensity in zip(mz, intensity):
                    ion_round = round(single_ion, 3)
                    if ion_round not in data:
                        data[ion_round] = []
                    data[ion_round].append((time_point, single_intensity)) 

            # adding missing intensities, so every m/z will have an intensity for every timepoint (quite unefficient algorithm used)
            copy_data = data.copy()
            for key, value in copy_data.items():
                np_value = np.array(value)
                for timepoint in timepoints:
                    if not timepoint in np_value[:, 0]:
                        data[key].append((timepoint, 0))
                data[key].sort()


            with open(directory_processed+"\\"+experiment_num+".p", 'wb') as fp:
                print(experiment_num, len(data))
                pickle.dump(data, fp, protocol=pickle.HIGHEST_PROTOCOL)
                

    