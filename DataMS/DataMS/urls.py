from django.contrib import admin
from django.urls import path
from django.conf import settings
from AppData import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('sort/<slug:filename>/', views.sort, name='sort'),
    path('admin/', admin.site.urls), 
]

#TODO: fix this - setup Nginx
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)