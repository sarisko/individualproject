from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect
from DataMS.settings import MEDIA_ROOT
import os, random, string
import json
import numpy as np
from numpy import trapz
# from pycaret.anomaly import *
from PythoMS.pythoms.mzml import mzML, extract_spectrum
import xlsxwriter
from pathlib import Path

def handle_uploaded_file(f):
    generated_filename = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    full_filename = os.path.join(MEDIA_ROOT, generated_filename+"."+f.name.split(".")[-1])
    # if media root neexistuje tak to vytvorit 
    media_path = Path(MEDIA_ROOT)
    media_path.mkdir(parents=True, exist_ok=True)
    with open(full_filename, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return generated_filename


class IndexView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'AppData/index.html')

    def post(self, request, *args, **kwargs):
        print(request.FILES, request.FILES["sample"], request.POST)
        filename = handle_uploaded_file(request.FILES["sample"])
        return HttpResponseRedirect('/sort/'+filename)


# def format_data(unpickled_data):
#     cols = ['id_mz'] + [str(i[0]) for i in unpickled_data[list(unpickled_data.keys())[0]]]
#     processed_data = []
#     for key, value in unpickled_data.items():
#         intens = [i[1] for i in value]
#         processed_data.append([key] + intens)

#     df = pd.DataFrame(processed_data, columns=cols)
#     df.set_index('id_mz')
#     return df


# def create_specific_model(df, name):
#     pycaret_model = create_model(name)
#     # dataframe of the results
#     model_results = assign_model(pycaret_model)
#     # binary results (anomaly or not)
#     model_anomaly_dump = sorted(list(zip(model_results['id_mz'], model_results['Anomaly'])))
#     # only anomaly traces
#     model_anomaly = [i for i in model_anomaly_dump if i[1]==1]
#     anomaly_score_only = sorted(list(zip(model_results['id_mz'], model_results['Anomaly_Score'], model_results['Anomaly'])))
#     return anomaly_score_only, model_anomaly

def are_we_the_same(key_metric, top):
    results = []
    results_anomaly = []
    i = 0
    while len(results) < top+1:
        key = key_metric[i][0]
        insert = True
        for j in results:
            if abs(j-key) < 1:
                insert = False
        if insert:
            results.append(key)
        results_anomaly.append(key)
        i += 1
    return results, results_anomaly

def sort(request, filename):
    full_filename = os.path.join(MEDIA_ROOT, filename+".mzML")
    print(full_filename)
    mzml = mzML(full_filename)
    mzml.function_timetic()
    timepoints = mzml.functions[1]['timepoints']

    # data = {m/z:[(t, i), (t, i), ...], ...}
    data = {}

    for spectrum_element, time_point in zip(mzml.tree.getElementsByTagName('spectrum'), timepoints):
        mz, intensity = extract_spectrum(spectrum_element)
        for single_ion, single_intensity in zip(mz, intensity):
            ion_round = round(single_ion, 3)
            if ion_round not in data:
                data[ion_round] = []
            data[ion_round].append((time_point, single_intensity)) 

    # adding missing intensities, so every m/z will have an intensity for every timepoint (quite unefficient algorithm used)
    copy_data = data.copy()
    for key, value in copy_data.items():
        np_value = np.array(value)
        for timepoint in timepoints:
            if not timepoint in np_value[:, 0]:
                data[key].append((timepoint, 0))
        data[key].sort()

    # Least Zeros
    zeros_ratio = []
    for key, value in data.items():
        value_np = np.array(value)
        zeros = np.count_nonzero(value_np[:, 1] == 0)
        zeros_ratio.append([key, zeros])
        
    # lz = list(np.array(sorted(zeros_ratio, key=lambda x: x[1])[:30])[:, 0])
    lz = lz = sorted(zeros_ratio, key=lambda x: x[1])
    least_zeros, least_zeros_anomaly = are_we_the_same(lz, 40)

    # Greatest Area
    key_area = []
    for key, value in data.items():
        ys = [el[1] for el in value]
        key_area.append([key, trapz(ys, dx=0.016933)]) # needs to be changed! to something general
    ga = sorted(key_area, key=lambda x: x[1])[::-1]
    greatest_area, greatest_area_anomaly = are_we_the_same(ga, 40)

    ga_anomaly_format = []
    lz_anomaly_format = []
    for key in data.keys():
        if key in least_zeros_anomaly:
            lz_anomaly_format.append([key, 1])
        else:
            lz_anomaly_format.append([key, 0])
        if key in greatest_area_anomaly:
            ga_anomaly_format.append([key, 1])
        else:
            ga_anomaly_format.append([key, 0])

    sheet_filename = os.path.join(MEDIA_ROOT, filename+".xlsx")
    workbook = xlsxwriter.Workbook(sheet_filename)
    worksheet = workbook.add_worksheet()
    worksheet.write(0, 0, 'Least Zeros')
    worksheet.write(0, 1, 'Greatest Area')

    for i in range(40):
        worksheet.write(i+1, 0, least_zeros[i])
        worksheet.write(i+1, 1, greatest_area[i])
    workbook.close()

    # # ML sod
    # df = format_data(data)
    # ano1 = setup(df, session_id=123, log_experiment=True, experiment_name='individual project')
    # model = create_specific_model(df, "sod")
    # sod = sorted(model[0], key=lambda x: x[1])[::-1]

    context_dict = {"sample":json.dumps(data), 
                    "lz": least_zeros, "lz_anomaly": sorted(lz_anomaly_format),
                    "ga": greatest_area, "ga_anomaly": sorted(ga_anomaly_format),
                    'n':range(32), 
                    "filename":filename}

    # delete uploaded file
    os.remove(full_filename)

    return render(request, 'AppData/sort.html', context=context_dict)

