# IndividualProject - Sara Kutkova

## SORT: End-to-end framework for Selective Organisation of Reaction Traces

This repository will contain the code, time log and documentation of my individual dissertation project, which I am to complete during my 4th year at the University of Glasgow. The project is supervised by Dr Gerardo Aragon Camarasa, and with colaboration with McIndoe group (https://web.uvic.ca/~mcindoe/) from University of Victoria.

-----------------------------------

**Project Proposal:** The objective of this project is to find out whether it is possible to use modern methods such as machine learning to categorize the roles of particular molecules in chemical reactions. This is traditionally done using laborious manual inspection of the temporal behavior of each molecule (how its abundance changes with time during the reaction), and we would like to determine whether it is possible to speed up this process. The database provided by the McIndoe group from the University of Victoria contains hundreds of mass spectrometric experiments each with thousands of molecules. We are going to use unsupervised learning to divide the molecules and if this stage is successful, we will build a GUI so this tool can be used by mass spectrometry groups. This project includes a number of challenges - the database needs to be converted to a more suitable format, the data can be noisy and the evaluation of the end result needs to be done in collaboration with the chemists in the McIndoe group.

-----------------------------------

The main directory contains the Jupyter Notebooks that were used to develop, test and evaluate the anomaly detection algorithms. The Proc file contains the commands that are executed when the app is deployed to Heroku. DataMS folder is a Django application - the SORT tool. The requirements.txt contains all the libraries used in this project.
 
This repository also contains pickle files with processed MS data and .mzML files that can be used to test the features of the SORL framework.

The SORT tool is can be accessed at [msdexplore.herokuapp.com](https://msdexplore.herokuapp.com/)

## Django set-up guide
Please follow these steps to deploy the SORT tool locally (in the development mode). The prerequisites are python3 and pip.

1. Install requirements (we recommend to set up a virtual environment)
   ```sh
   pip install -r requirements.txt

2. Move to the folder with the Django app
   ```sh
   cd DataMS

3. Make migrations
   ```sh
   python manage.py makemigrations

4. Apply migrations
   ```sh
   python manage.py migrate

5. Collect static
   ```sh
   python manage.py collectstatic

6. Runserver
   ```sh
   python manage.py runserver

