# Timelog

* Unsupervised machine learning approaches to categorize the roles of particular molecules in chemical reactions
* Sara Kutkova
* 2406155
* Gerardo Aragon Camarasa

## Guidance

* This file contains the time log for your project. It will be submitted along with your final dissertation.
* **YOU MUST KEEP THIS UP TO DATE AND UNDER VERSION CONTROL.**
* This timelog should be filled out honestly, regularly (daily) and accurately. It is for *your* benefit.
* Follow the structure provided, grouping time by weeks.  Quantise time to the half hour.

## Week 1

### 21 Sept 2021

* *4 hours* reading the project guidance notes

## Week 2

### 28 Sept 2021

* *2 hours* lv 4 project information session
* *1 hour* hardware setup - separate dual boot laptop for testing

### 29 Sept 2021

* *2 hours* preparation for the first meeting with the supervisor, notes and desciption of the project
* *0.5 hour* meeting with supervisor

### 30 Sept 2021

* *0.5 hour* presenting this project to the McIndoe group, they will provide the data

## Week 3

### 6 Oct 2021

* *1 hour* creating a GitLab repository, added gitignore and logtimetable, nice first readme
* *0.5 hour* milestones for each month
* *0.5 hour* setup Zotero and bookmark some interesting articles

### 7 Oct 2021

* *2 hours* preparation for meeting with supervisor
* *0.5 hour* meeting with supervisor

## Week 4

### 13 Oct 2021

* *0.5 hour* preparation for meeting with supervisor (chemistry)
* *1 hour* meeting with supervisor (chemistry)
* *2 hours* download and look through Protheowizard and PythoMS
* *0.5 hour* preparation for meeting with supervisor

### 14 Oct 2021

* *1 hour* meeting with supervisor
* *2 hours* reading relevant articles
* *1 hour* administrative things

## Week 5

### 18 Oct 2021

* *4 hours* debugging pyhoMS

### 21 Oct 2021

* *0.5 hour* preparation for meeting with supervisor
* *0.5 hour* meeting with supervisor
* *0.5 hour* comunication with Lard (creator of PythoMS)
* *0.5 hour* comunication with supervisor (chemistry) - articles
* *2 hours* brainstorming the codebase

## Week 6

### 25 Oct 2021

* *0.5 hour* preparation for meeting with supervisor (chemistry)
* *1 hour* meeting with supervisor (chemistry)

### 27 Oct 2021

* *1.5 hour* getting the timestamp from MZML class

### 28 Oct 2021

* *1 hour* preparation for meeting with supervisor
* *2 hours* writing extract raw data script
* *1 hour* administrative things

## Week 7

### 1 Nov 2021

* *1.5 hour* timelog technical debt

### 4 Nov 2021

* *1 hour* reading through pyopems
* *0.5 hour* preparation for meeting with supervisor
* *0.5 hour* meeting with supervisor
* *2 hours* automation of extraction script - will look for .raw files are process them

## Week 8

### 8 Nov 2021

* *1 hour* writing a polite request for more MS data into McIndoe group, One drive for backup of the data
* *0.5 hour* preparation for meeting with supervisor (chemistry)
* *0.5 hour* meeting with supervisor (chemistry)
* *1 hour* visualisation of first version of MS data in jupyter notebook
* *1 hour* adding missing intensities, so every m/z will have an intensity for every timepoint
* *1.5 hour* least 0 intensities alsogithms used on data, visualisation

### 9 Nov 2021

* *1 hour* documentation of code, refactoring, json to pickle
* *0.5 hour* comunication with Lars (creator of PythoMS)
* *1 hour* Gitlab clean up, new milestone created

### 10 Nov 2021

* *1 hour* communication with Lars about cofiguration of Proteowizard location in mzml.py
* *1 hour* preparation for meeting with supervisor

### 11 Nov 2021

* *0.5 hour* meeting with supervisor

## Week 9

### 18 Nov 2021
* *0.5 hour* preparation for meeting with supervisor
* *1 hour* exploring the feature extraction
* *0.5 hour* meeting with supervisor
* *1.5 hour* making the presentation and presenting my results so far in front of the McIndoe group

